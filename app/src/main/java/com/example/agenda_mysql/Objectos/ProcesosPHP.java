package com.example.agenda_mysql.Objectos;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.agenda_mysql.ListActivity;
import com.example.agenda_mysql.MainActivity;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProcesosPHP implements  Response.ErrorListener,Response.Listener<JSONObject> {

     public String URLServer="urlserver";
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> contactos = new ArrayList<Contactos>();
    private String serverip = "https://wilddeer.000webhostapp.com/WebService/";

    public String getURLServer() {
        return URLServer;
    }

    public void setURLServer(String URLServer) {
        this.URLServer = URLServer;
    }

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void insertarContactoWebService(Contactos c) {
        String url = serverip + "wsRegistro.php?nombre=" + c.getNombre() + "&tel1=" + c.getTelefono1() + "&tel2=" + c.getTelefono2() + "&direccion=" + c.getDireccion() + "&notas=" + c.getNotas() + "&favorite=" + c.getFavorite() + "&idmovil=" + c.getIdMovil();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void actualizarContactoWebService(Contactos c, int id) {
        String url = serverip + "wsActualizar.php?id=" + id + "&nombre=" + c.getNombre() + "&direccion=" + c.getDireccion() + "&tel1=" + c.getTelefono1() + "&tel2=" + c.getTelefono2() + "&notas=" + c.getNotas() + "&favorite=" + c.getFavorite();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void borrarContactoWebService(int id) {
        String url = serverip + "wsEliminar.php?id=" + id;
          setURLServer(url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);

    }

    public String borrar(int id){

        String url = serverip + "wsEliminar.php?id=" + id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return url;

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());     }

    @Override
    public void onResponse(JSONObject response) {
    }
    }